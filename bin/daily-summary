#!/usr/bin/perl -w
#
# daily-summary
#
# Grab various stats from the wiki
#

use strict;

my $base = "/srv/wiki.debian.org";
my $log = "$base/var/log/account-creation-check";
my $num_days = 7; # Summarise the last week
my $pattern_threshold = 10; # Print anything that occurs at least this many times
my $end_date = get_date(time());
my $start_date = get_date(time() - (86400 * $num_days));

my $allow = 0;
my $deny = 0;
my $total = 0;
my %analysis;

sub get_date
{
    my $in = shift;
    my @tm;
    my $text;
    @tm = gmtime($in);
    $text = sprintf("%4d-%02d-%02d", (1900 + $tm[5]),(1 + $tm[4]),$tm[3]);
    return $text;
}

sub percent ($$)
{
    my $numerator = shift;
    my $denominator = shift;
    my $text = sprintf("%.2f%%", 100 * $numerator / $denominator);
    return $text;
}

sub analyse
{
    my $tmp = shift;
    my %e = %$tmp;
    my @email_parts = split /\@/, $e{"email"}, 2;
    my @addrs;
    my $net;

    $analysis{"username"}{$e{"username"}}++;
    $analysis{"email"}{$e{"email"}}++;
    $analysis{"email_local_part"}{$email_parts[0]}++;
    if (scalar(@email_parts) > 1) {
        $analysis{"email_domain"}{$email_parts[1]}++;
    }

    # Check for IPv4
    if ($e{"host"} =~ m/\d+\.\d+\.\d+\.\d+/) {
	@addrs = split /\./,$e{"host"};
	$net = "$addrs[0].$addrs[1].$addrs[2].0";
	$analysis{"class C"}{$net}++;
	$net = "$addrs[0].$addrs[1].0.0";
	$analysis{"class B"}{$net}++;
	$net = "$addrs[0].0.0.0";
	$analysis{"class A"}{$net}++;
    }
    # We don't yet get enough IPv6 traffic to be worth analysing
    # yet...

    $analysis{"host"}{$e{"host"}}++;

    @addrs = split /\./,$e{"rdns"};
    for (my $i = 0; $i < scalar(@addrs); $i++) {
	$net = "";
	for (my $j = $i; $j < scalar(@addrs); $j++) {
	    $net .= "$addrs[$j]";
	    if ($j < scalar(@addrs) - 1) {
		$net .= ".";
	    }
	}
	$analysis{"network"}{$net}++;
    }
}    

# First, what account creation attempts have happened in the last 24
# hours? Any patterns we can detect?
my $in_date = 0;
my %entry;
open (LOG, "< $log") or die "Can't open $log for reading: $!\n";
while (my $line = <LOG>) {
    chomp $line;
    if (!$in_date) {
	if ($line =~ m/^\Q$start_date\E/) {
	    $in_date = 1;
	}
    }
    if ($in_date) {
	if ($line =~ m/^\Q$end_date\E/) {
	    if (%entry) {
		analyse(\%entry);
	    }
	    $in_date = 0;
	    last;
	}
	if ($line =~ m/^(\Q$start_date\E.*UTC)/) {
	    if (%entry) {
		analyse(\%entry);
	    }
	    $entry{"date"} = $1;
	} elsif ($line =~ m/^  username:\s+(.*)/) {
	    $entry{"username"} = $1;
	} elsif ($line =~ m/^  email:\s+(.*)/) {
	    $entry{"email"} = $1;
	} elsif ($line =~ m/^  host:\s+(.*)\s+\((.*)\)/) {
	    $entry{"host"} = $1;
	    $entry{"rdns"} = $2;
	} elsif ($line =~ m/^  DENY:\s+total score (\d+)/) {
	    $deny++;
	    $entry{"allow"} = 0;
	    $entry{"score"} = $1;
	} elsif ($line =~ m/^  ALLOW:\s+total score (\d+)/) {
	    $allow++;
	    $entry{"allow"} = 1;
	    $entry{"score"} = $1;
	}
    }
}
close LOG;

$total = $allow + $deny;
print "In period covering $start_date to $end_date:\n";
print "Number of account creation attempts: $total\n";
print "  allowed: $allow (" . percent($allow, $total) .")\n";
print "  denied:  $deny (" . percent($deny, $total) .")\n\n";

print "Further breakdown (listing any with >= $pattern_threshold attempts):\n";
foreach my $k1 (sort keys %analysis) {
    print "  by $k1: \n";
    my $tmp = $analysis{$k1};
    my %e = %$tmp;
    my $count = 0;
    foreach my $k2 (sort {$e{$b} <=> $e{$a}} keys %e) {
	if ($e{$k2} >= $pattern_threshold) {
	    print "    $k2 (" . $e{$k2} . ", " . percent($e{$k2}, $total) . ")\n";
	    $count++;
	}
    }
    if (!$count) {
	print "    No notable patterns\n";
    }
}
