Migration finalisation plan

Disable editing on old wiki - edit rietz:wikiconfig.py

start DSA
 stop widor apache/wsgi instances (don't want them messing with cache)
 rm -rf underlay.old data-new.luca
 rsync rietz:/srv/wiki.debian.org/www/data widor:/srv/wiki.debian.org/www/data-old
end DSA

sudo -u wiki -s

cp -a data-old data

setup permissions properly (u+rwX g+rwX g+s for dirs)

edit www/data/pages/CategoryPresentation/edit-log
the last line is broken, it should be part of the previous line

crashes loading the main edit-log due to a memory error
so move the edit-log out the way

same for the event-log

./migrate.py --config-dir=/srv/wiki.debian.org/www/ --wiki-url=wiki.debian.org migration data

restore last 90 days worth of edit-log & event-log for RecentChanges

rm -f /srv/wiki.debian.org/www/data/intermap.txt

./migrate.py --config-dir=/srv/wiki.debian.org/www/ --wiki-url=wiki.debian.org maint cleancache

start DSA:
 start widor apache/wsgi instances
 update DNS
end DSA

enable textchas? https://moinmo.in/HelpOnTextChas
<ThomasWaldmann> pabs3: dont forget the notextcha group for the regular editors not getting annoyed too much

<ThomasWaldmann> but they should get some note on the front page about the changes, to read HelpOnLinking HelpOnMacros etc.
