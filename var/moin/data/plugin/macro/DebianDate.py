from MoinMoin import wikiutil

suites = set(['oldstable', 'stable', 'testing', 'unstable', 'experimental'])

def execute(macro, suite):
	if suite in suites:
		with open('/srv/wiki.debian.org/var/apt/%s.date' % suite, 'r') as date_file:
			return macro.formatter.rawHTML(wikiutil.escape(date_file.read().strip()))
	return ''
